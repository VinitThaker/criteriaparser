package com.vinit.criteriaparser.api

import androidx.lifecycle.LiveData
import com.vinit.criteriaparser.model.ScanData
import com.vinit.criteriaparser.model.network.Resource
import retrofit2.http.GET

interface ApiService {

    @GET("data")
    fun getScanData(): LiveData<Resource<List<ScanData>>>

}
