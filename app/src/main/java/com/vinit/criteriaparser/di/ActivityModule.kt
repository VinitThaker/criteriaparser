package com.vinit.criteriaparser.di

import com.vinit.criteriaparser.ui.fragment.ListFragment
import com.vinit.criteriaparser.ui.activity.MainActivity
import com.vinit.criteriaparser.ui.fragment.DetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun provideListFragment(): ListFragment

    @ContributesAndroidInjector
    internal abstract fun provideDetailFragment(): DetailFragment
}
