package com.vinit.criteriaparser.di.base

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
