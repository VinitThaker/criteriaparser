package com.vinit.criteriaparser.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Criteria(
    val text: String,
    val type: String
): Parcelable