package com.vinit.criteriaparser.repo

import androidx.lifecycle.LiveData
import com.vinit.criteriaparser.api.ApiService
import com.vinit.criteriaparser.model.ScanData
import com.vinit.criteriaparser.model.network.Resource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ScanDataRepository @Inject constructor(
        private val apiService: ApiService
) {

    fun getScanData(): LiveData<Resource<List<ScanData>>> {
        //we can use NetworkBoundResource if we are using room to store data and keep single source of truth
        return apiService.getScanData()
    }

}