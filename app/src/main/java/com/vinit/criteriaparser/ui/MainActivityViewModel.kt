package com.vinit.criteriaparser.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vinit.criteriaparser.Event
import com.vinit.criteriaparser.model.ScanData
import com.vinit.criteriaparser.model.network.Resource
import com.vinit.criteriaparser.repo.ScanDataRepository
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
        scanDataRepository: ScanDataRepository
) : ViewModel() {

    private var scanDataList: LiveData<Resource<List<ScanData>>> = scanDataRepository.getScanData()
    fun getListOfScanData() = scanDataList

    private val _scan = MutableLiveData<Event<ScanData>>()
    val scan: LiveData<Event<ScanData>>
        get() = _scan

    internal fun openScan(scanData: ScanData) {
        _scan.value = Event(scanData)
    }

}