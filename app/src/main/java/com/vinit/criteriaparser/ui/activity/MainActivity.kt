package com.vinit.criteriaparser.ui.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import com.vinit.criteriaparser.Event
import com.vinit.criteriaparser.R
import com.vinit.criteriaparser.model.ScanData
import com.vinit.criteriaparser.ui.fragment.ListFragment
import com.vinit.criteriaparser.ui.MainActivityViewModel
import com.vinit.criteriaparser.ui.base.BaseActivity
import com.vinit.criteriaparser.ui.fragment.DetailFragment
import com.vinit.criteriaparser.utils.getViewModel
import com.vinit.criteriaparser.utils.replaceFragmentInActivity

class MainActivity : BaseActivity() {

    private val mainActivityViewModel by lazy { getViewModel<MainActivityViewModel>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupListFragment()

        mainActivityViewModel.scan.observe(this, Observer<Event<ScanData>> { event ->
            event.getContentIfNotHandled()?.let {
                openScanDetail(it)
            }
        })

    }

    private fun openScanDetail(scanData: ScanData) {
        replaceFragmentInActivity(DetailFragment.newInstance(scanData), R.id.contentFrame, true)
    }

    private fun setupListFragment() {
        supportFragmentManager.findFragmentById(R.id.contentFrame)
            ?: replaceFragmentInActivity(ListFragment.newInstance(), R.id.contentFrame, false)
    }
}
