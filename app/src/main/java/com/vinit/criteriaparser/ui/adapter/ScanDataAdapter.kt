package com.vinit.criteriaparser.ui.adapter

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vinit.criteriaparser.R
import com.vinit.criteriaparser.model.ScanData
import com.vinit.criteriaparser.utils.inflate
import kotlinx.android.synthetic.main.scan_data_layout.view.*

class ScanDataAdapter(
        private val listener: (ScanData) -> Unit
) : RecyclerView.Adapter<ScanDataAdapter.ScanDataHolder>() {

    private var scanDataList: List<ScanData> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ScanDataHolder(parent.inflate(R.layout.row_scan_data))

    override fun onBindViewHolder(scanDataHolder: ScanDataHolder, position: Int) = scanDataHolder.bind(scanDataList[position], listener)

    override fun getItemCount() = scanDataList.size

    class ScanDataHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(scanData: ScanData, listener: (ScanData) -> Unit) = with(itemView) {
            tvScanName.text = scanData.name
            tvScanTag.text = scanData.tag
            tvScanTag.setTextColor(Color.parseColor(scanData.color))
            setOnClickListener { listener(scanData) }
        }

    }

    fun replaceItems(items: List<ScanData>) {
        scanDataList = items
        notifyDataSetChanged()
    }
}