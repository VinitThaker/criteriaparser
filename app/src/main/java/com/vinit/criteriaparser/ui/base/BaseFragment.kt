package com.vinit.criteriaparser.ui.base

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.vinit.criteriaparser.di.base.Injectable
import com.vinit.criteriaparser.ui.MainActivityViewModel
import javax.inject.Inject

open class BaseFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var mainActivityViewModel: MainActivityViewModel

}