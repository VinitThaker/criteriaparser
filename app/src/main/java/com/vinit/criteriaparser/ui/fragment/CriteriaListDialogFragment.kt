package com.vinit.criteriaparser.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.vinit.criteriaparser.R
import com.vinit.criteriaparser.model.Criteria
import kotlinx.android.synthetic.main.fragment_criteria_list_dialog.*
import kotlinx.android.synthetic.main.fragment_criteria_list_dialog_item.view.*

class CriteriaListDialogFragment : BottomSheetDialogFragment() {
    private var mListener: Listener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_criteria_list_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.layoutManager = LinearLayoutManager(context)
        list.adapter = CriteriaAdapter(arguments?.getParcelableArrayList<Criteria>(ARG_CRITERIAL_LIST)!!)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        mListener = if (parent != null) {
            parent as Listener
        } else {
            context as Listener
        }
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }

    interface Listener {
        fun onCriteriaClicked(criteria: Criteria)
    }

    private inner class ViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.fragment_criteria_list_dialog_item, parent, false))

    private inner class CriteriaAdapter internal constructor(private val criteriaList: ArrayList<Criteria>) :
        RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            criteriaList[position].let {
                holder.itemView.text.text = it.text
                holder.itemView.text.tag = it
                holder.itemView.text.setOnClickListener { view ->
                    mListener?.onCriteriaClicked(view.tag as Criteria)
                    dismiss()
                }
            }
        }

        override fun getItemCount(): Int {
            return criteriaList.size
        }
    }

    companion object {
        const val ARG_CRITERIAL_LIST = "creteria_list"

        fun newInstance(criteriaList: ArrayList<Criteria>): CriteriaListDialogFragment =
            CriteriaListDialogFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(ARG_CRITERIAL_LIST, criteriaList)
                }
            }
    }
}
