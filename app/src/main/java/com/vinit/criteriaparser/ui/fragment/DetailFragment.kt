package com.vinit.criteriaparser.ui.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.vinit.criteriaparser.Event
import com.vinit.criteriaparser.R
import com.vinit.criteriaparser.model.Criteria
import com.vinit.criteriaparser.model.ScanData
import com.vinit.criteriaparser.ui.MainActivityViewModel
import com.vinit.criteriaparser.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.scan_data_layout.*

class DetailFragment : BaseFragment(), CriteriaListDialogFragment.Listener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //sharing ViewModel with activity scope
        mainActivityViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(MainActivityViewModel::class.java)

        mainActivityViewModel.scan.observe(this, Observer<Event<ScanData>> { event ->
            val scanData = event.peekContent()
            tvScanName.text = scanData.name
            tvScanTag.text = scanData.tag
            tvScanTag.setTextColor(Color.parseColor(scanData.color))

            tvScanCriteria.text = scanData.criteria[0].text
            tvScanCriteria.setOnClickListener {
                CriteriaListDialogFragment.newInstance(scanData.criteria).show(childFragmentManager, TAG)
            }
        })
    }

    override fun onCriteriaClicked(criteria: Criteria) {
        tvScanCriteria.text = criteria.text
    }

    companion object {
        const val ARGUMENT_SCAN_DATA = "SCAN_DATA"
        const val TAG = "DetailFragment"

        fun newInstance(scanData: ScanData) = DetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARGUMENT_SCAN_DATA, scanData)
            }
        }
    }
}
