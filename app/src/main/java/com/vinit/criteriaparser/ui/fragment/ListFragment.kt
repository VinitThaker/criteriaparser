package com.vinit.criteriaparser.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.vinit.criteriaparser.R
import com.vinit.criteriaparser.ui.MainActivityViewModel
import com.vinit.criteriaparser.ui.adapter.ScanDataAdapter
import com.vinit.criteriaparser.ui.base.BaseFragment
import com.vinit.criteriaparser.utils.load
import com.vinit.criteriaparser.utils.observe
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.progress_layout.*

class ListFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //sharing ViewModel with activity scope
        mainActivityViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(MainActivityViewModel::class.java)

        //setting up RecycleView and adapter
        scan_list.setEmptyView(empty_view)
        scan_list.setProgressView(progress_view)

        val adapter = ScanDataAdapter { scanData ->
            mainActivityViewModel.openScan(scanData)
            Log.d("beat", "${scanData.name} clicked")
        }
        scan_list.adapter = adapter
        scan_list.layoutManager = LinearLayoutManager(context)

        // Observing for data change
        mainActivityViewModel.getListOfScanData().observe(this) {
            it.load(scan_list) {
                // Update the UI as the data has changed
                it?.let { adapter.replaceItems(it) }
            }
        }
    }

    companion object {
        fun newInstance() = ListFragment()
        private const val TAG = "ListFragment"
    }

}
